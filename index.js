const serve = require('koa-static');
const koa = require('koa');
const app = new koa();
const port = process.env.PORT || 8080;

app.use(serve('./build'));

app.listen(port);